package org.example.fling.magic.repo;

import java.util.List;
import java.util.UUID;

public interface CrudRepository<Entity> {
    List<Entity> findAll();

   Entity findById(UUID uuid);
}
