package org.example.fling.setup;

public class ApplicationProps {
    private String connection;
    private String username;
    private String password;

    public ApplicationProps(String connection, String username, String password) {
        this.connection = connection;
        this.username = username;
        this.password = password;
    }
    public String getConnection() {
        return connection;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

