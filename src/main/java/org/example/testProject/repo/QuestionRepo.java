package org.example.testProject.repo;

import org.example.testProject.entities.Question;
import org.example.fling.annotations.repo.FlingRepo;
import org.example.fling.magic.repo.CrudRepository;

@FlingRepo
public interface QuestionRepo extends CrudRepository<Question> {
}
