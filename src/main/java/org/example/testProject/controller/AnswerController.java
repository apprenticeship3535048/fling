package org.example.testProject.controller;

import org.example.fling.annotations.controller.PathVariable;
import org.example.testProject.entities.Answer;
import org.example.fling.annotations.controller.mapping.GetMapping;
import org.example.fling.annotations.controller.mapping.RestController;
import org.example.testProject.service.AnswerService;

import java.util.List;
import java.util.UUID;

@RestController(mapping = "/answer", clazz = Answer.class)
public class AnswerController {

    private final AnswerService service;

    public AnswerController(AnswerService service) {
        this.service = service;
    }

    @GetMapping(mapping = "/all")
    public List<Answer> findAll(){
        return service.findAll();
    }

    @GetMapping(mapping = "/{id}")
    public Answer findById(@PathVariable(name = "id") UUID id){
        return service.findById(id);
    }
}
