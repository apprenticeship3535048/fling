package org.example.testProject.service;

import org.example.testProject.entities.Question;
import org.example.testProject.repo.QuestionRepo;

import java.util.List;

public class QuestionService {
    private final QuestionRepo repo;

    public QuestionService(QuestionRepo repo) {
        this.repo = repo;
    }

    public List<Question> findAll() {
        return repo.findAll();
    }
}
