package org.example.fling;

import org.example.fling.annotations.entity.Column;

import java.util.*;

public class Entity {
    @Column
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
