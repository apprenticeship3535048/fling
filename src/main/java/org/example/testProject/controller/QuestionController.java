package org.example.testProject.controller;

import org.example.testProject.entities.Question;
import org.example.fling.annotations.controller.mapping.GetMapping;
import org.example.fling.annotations.controller.mapping.RestController;
import org.example.testProject.service.QuestionService;

import java.util.List;

@RestController(mapping = "/q", clazz = Question.class)
public class QuestionController {
    private final QuestionService service;

    public QuestionController(QuestionService service) {
        this.service = service;
    }

    @GetMapping(mapping = "/all")
    public List<Question> findAll() {
        return service.findAll();
    }
}
