package org.example.fling.setup.server;

import com.sun.net.httpserver.HttpExchange;
import org.example.fling.annotations.controller.PathVariable;
import org.example.fling.magic.JsonConverter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.List;
import java.util.UUID;

public class HandlerService {
    public static Object[] getArgs(Parameter[] parameters, HttpExchange exchange) {
        Object[] args = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            if (parameters[i].isAnnotationPresent(PathVariable.class)) {
                if (parameters[i].getType().equals(UUID.class)) {
                    String requestUri = exchange.getRequestURI().toString();
                    requestUri = requestUri.replaceAll("/\\w*/", "");
                    UUID uuid = UUID.fromString(requestUri);
                    args[i] = uuid;
                }
            }
        }
        return args;
    }

    public static String createResponse(Object controller, Object[] args, Method method) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        String response;
        Object result = method.invoke(controller, args);
        if (result instanceof List) {
            response = JsonConverter.convertListOfObjectsToJson((List<?>) result);
        } else response = JsonConverter.convertObjectToJson(result);
        return response;
    }
}
