package org.example.fling.annotations;

import org.example.fling.annotations.controller.mapping.RestController;
import org.example.fling.annotations.repo.FlingRepo;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AnnotationScanner {
    private static final String mainClassName = System.getProperty("sun.java.command");

    private static final List<Class<?>> annotatedClasses;

    static {
        try {
            annotatedClasses = getAllClasses(Class.forName(mainClassName).getPackageName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Class<?>> controllers = getControllerClasses();
    public static List<Class<?>> services = getServiceClasses();
    public static List<Class<?>> repositories = getRepositoryInterfaces();

    public static List<Class<?>> getControllerClasses() {
        List<Class<?>> classes = annotatedClasses;
        List<Class<?>> controllers = new ArrayList<>();
        for (Class<?> clazz : classes) {
            if (clazz.isAnnotationPresent(RestController.class)) {
                controllers.add(clazz);
            }
        }
        return controllers;
    }

    public static List<Class<?>> getServiceClasses() {
        List<Class<?>> classes = annotatedClasses;
        List<Class<?>> controllers = new ArrayList<>();
        for (Class<?> clazz : classes) {
            if (clazz.isAnnotationPresent(FlingService.class)) {
                controllers.add(clazz);
            }
        }
        return controllers;
    }

    public static List<Class<?>> getRepositoryInterfaces() {
        List<Class<?>> classes = annotatedClasses;
        List<Class<?>> repos = new ArrayList<>();
        for (Class<?> clazz : classes) {
            if (clazz.isAnnotationPresent(FlingRepo.class) && clazz.isInterface()) {
                repos.add(clazz);
            }
        }
        return repos;
    }

    private static List<Class<?>> getAllClasses(String pkg) {
        String path = pkg.replace(".", "/");
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        List<Class<?>> classes = new ArrayList<>();
        try {
           File file1 = new File(Collections.list(loader.getResources(path)).get(0).getFile());
           if (file1.isDirectory()) {
               classes.addAll(scanDirectory(pkg, file1));
           }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return classes;
    }

    private static List<Class<?>> scanDirectory(String pkg, File directory) {
        List<Class<?>> classes = new ArrayList<>();
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory() && !file.getName().equals("fling")) {
                    classes.addAll(scanDirectory(pkg + "." + file.getName(), file));
                } else if (file.getName().endsWith(".class")) {
                    String className = pkg + "." + file.getName().substring(0, file.getName().length() - 6);
                    try {
                        Class<?> clazz = Class.forName(className);
                        if (clazz.getAnnotations().length > 0) {
                            classes.add(clazz);
                        }
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return classes;
    }

    @Retention(RetentionPolicy.RUNTIME)
    public static @interface Query {
        String query();
    }
}
