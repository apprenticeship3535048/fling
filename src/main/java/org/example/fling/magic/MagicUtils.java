package org.example.fling.magic;

import org.example.fling.annotations.entity.Column;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MagicUtils {
    public static <T> Method getSetter(Field field, T entity) throws NoSuchMethodException {
        String setterName = "set" + capitalize(field.getName());
        return entity.getClass().getMethod(setterName, field.getType());
    }
    public static <T> Method getGetter(Field field, T entity) throws NoSuchMethodException {
        String getterName;
        if (field.getType() == boolean.class) getterName = "is" + capitalize(field.getName());
        else getterName = "get" + capitalize(field.getName());
        return entity.getClass().getMethod(getterName);
    }

    public static String capitalize(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static UUID getUUIDFromBytes(byte[] bytes) {
        if (bytes.length != 16) {
            throw new IllegalArgumentException("Not a UUID");
        }
        long mostSigBits = 0;
        long leastSigBits = 0;
        for (int i = 0; i < 8; i++) {
            mostSigBits = (mostSigBits << 8) | (bytes[i] & 0xff);
        }
        for (int i = 8; i < 16; i++) {
            leastSigBits = (leastSigBits << 8) | (bytes[i] & 0xff);
        }
        return new UUID(mostSigBits, leastSigBits);
    }

    /**
     * This only provides the fields of the class and it's parent class-
     * Todo: add recursive field retrieval/ take another look
     */
    public static List<Field> getAllFields(Class<?> clazz) {
        List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        Class<?>[] interfaces = clazz.getInterfaces();

        for (Class<?> inter : interfaces) {
            fields.addAll(Arrays.asList(inter.getDeclaredFields()));
        }
        Class<?> superClass = clazz.getSuperclass();
        if (null != superClass) {
            fields.addAll(getAllFields(superClass));
        }
        fields.removeIf(field -> !field.isAnnotationPresent(Column.class));
        return fields;
    }

    public static String[] extractColumnNames(Class<?> clazz) {
        List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        Class<?>[] interfaces = clazz.getInterfaces();

        for (Class<?> inter : interfaces) {
            fields.addAll(Arrays.asList(inter.getDeclaredFields()));
        }
        Class<?> superClass = clazz.getSuperclass();
        if (null != superClass) {
            fields.addAll(getAllFields(superClass));
        }
        fields.removeIf(field -> !field.isAnnotationPresent(Column.class));
        String[] fieldNames = new String[fields.size()];
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            Column annotation = field.getAnnotation(Column.class);
            String columnName = annotation.name().isEmpty() ? field.getName() : annotation.name();
            fieldNames[i] = columnName;
        }
        return fieldNames;
    }
}
