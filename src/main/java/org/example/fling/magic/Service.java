package org.example.fling.magic;

import org.example.fling.Entity;
import org.example.fling.setup.sql.Connector;

import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Service<T extends Entity> {

    private final Connector connector;
    public Class<T> type;

    public Service(Connector connector, Class<T> type) {
        this.connector = connector;
        this.type = type;
    }

    public List<T> findAll() {
        try {
            ResultSet resultSet = connector.executeQuery(getType());
            List<T> resultList = new ArrayList<>();
            while (resultSet.next()) {
                Map<String, Object> results = new HashMap<>();
                for (int i = 1; i < resultSet.getMetaData().getColumnCount() + 1; i++) {
                    String columnName = resultSet.getMetaData().getColumnName(i);
                    Object columnValue = resultSet.getObject(i);
                    results.put(columnName, columnValue);
                }
                //T entity = Magician.buildEntityFromQueryResult(results, type.getDeclaredConstructor().newInstance());
                //resultList.add();
            }
            return resultList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public Class<T> getType() {
        return type;
    }
}
