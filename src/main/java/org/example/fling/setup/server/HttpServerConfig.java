package org.example.fling.setup.server;

import com.sun.net.httpserver.HttpServer;
import org.example.fling.annotations.AnnotationScanner;
import org.example.fling.annotations.controller.mapping.GetMapping;
import org.example.fling.annotations.controller.mapping.RestController;
import org.example.fling.setup.AppPropsService;

import java.io.IOException;
import java.lang.reflect.*;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.example.fling.magic.Injector.construct;

public class HttpServerConfig {

    /**
     * Creates the server on the port specified in the application props
     */
    public static void createServer() throws IOException {
        int port = Integer.parseInt(AppPropsService.getProps().getProperty("port"));
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        createHandlers(server);
        server.start();
        System.out.println("Listening on Port " + port);
    }
    /**
     * Creates the server context (the endpoints which can be reached) -
     * loops through all endpoints detected and paris them with the functions called for them.
     */
    private static void createHandlers(HttpServer server) {
        for (Class<?> controller : Objects.requireNonNull(AnnotationScanner.getControllerClasses())) {
            List<Method> methods = new ArrayList<>(List.of(controller.getMethods()));
            Class<?> superclass = controller.getSuperclass();
            while (superclass != null) {
                Method[] superMethods = superclass.getDeclaredMethods();
                methods.addAll(List.of(superMethods));
                superclass = superclass.getSuperclass();
            }
            createContextForControllerSuperMethods(methods, controller, server);
        }
    }

    /**
     * Creates the server context (the endpoints which can be reached) - pairs mappings and their functionality
     */
    private static void createContextForControllerSuperMethods(List<Method> methods, Class<?> controller, HttpServer server) {
        for (Method method : methods) {

            if (method.isAnnotationPresent(GetMapping.class)) {
                RestController controllerAnnotation = controller.getAnnotation(RestController.class);
                String mapping = controllerAnnotation.mapping() + method.getAnnotation(GetMapping.class).mapping();
                mapping = mapping.replaceAll("\\{[^}]*}","");
                Object controllerInstance = construct(controller, controllerAnnotation.clazz());
                server.createContext(mapping, new DefaultHandler(method, controllerInstance));
            }


        }
    }

}


