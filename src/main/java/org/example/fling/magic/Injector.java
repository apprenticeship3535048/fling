package org.example.fling.magic;

import org.example.fling.Entity;
import org.example.fling.annotations.repo.FlingRepo;
import org.example.fling.magic.repo.DynamicInvocationHandler;
import org.example.fling.setup.AppPropsService;
import org.example.fling.setup.server.DefaultHandler;
import org.example.fling.setup.sql.Connector;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Injector<E extends Entity> {

    public Service<E> injectService(Class<E> clazz) throws SQLException, IOException, ClassNotFoundException {
        return new Service<>(new Connector(new AppPropsService()), clazz);
    }

    //private Constructor<?> getConstructor() {
    //    return new Constructor<>();
    //}
    public static Object construct(Class<?> clazz, Class<?> entity) {
        try {
            if (clazz.isAnnotationPresent(FlingRepo.class)) {
                return Proxy.newProxyInstance(
                        DefaultHandler.class.getClassLoader(),
                        new Class[] {clazz},
                        new DynamicInvocationHandler()
                );
            }
            Constructor<?>[] constructors = clazz.getDeclaredConstructors();
            Constructor<?> constructor = null;
            List<List<Class<?>>> parameterArray = new ArrayList<>();
            List<Constructor<?>> validConstructors = new ArrayList<>();
            for (Constructor<?> loopedConstructor : constructors) {
                Class<?>[] parameters = loopedConstructor.getParameterTypes();
                if (parameters.length > 0) {
                    validConstructors.add(loopedConstructor);
                    parameterArray.add(List.of(parameters));
                    break;
                }
                constructor = loopedConstructor;
            }
            assert null != constructor;
            List<Class<?>> parameters = findLongestArray(parameterArray);
            if (!validConstructors.isEmpty()) constructor = validConstructors.get(findLongestArrayIndex(parameterArray));
            if (parameters.isEmpty()) {
                return constructor.newInstance();
            } else {
                Object[] instantiatedParameters = new Object[parameters.size()];
                for (int i = 0; i < parameters.size(); i++) {
                    Class<?> parameterClass = parameters.get(i);
                    if (parameterClass == Class.class) {
                        instantiatedParameters[i] = entity;
                    } else {
                        instantiatedParameters[i] = construct(parameterClass, entity);
                    }
                }
                return constructor.newInstance(instantiatedParameters);
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Class<?>> findLongestArray(List<List<Class<?>>> arrayOfArrays) {
        int maxLength = 0;
        List<Class<?>> longestArray = new ArrayList<>();

        for (List<Class<?>> array : arrayOfArrays) {
            if (array == null) continue;
            if (array.size() > maxLength) {
                maxLength = array.size();
                longestArray = array;
            }
        }
        return longestArray;
    }
    private static int findLongestArrayIndex(List<List<Class<?>>> arrayOfArrays) {
        int maxLength = 0;
        int index = 0;

        for (int i = 0; i < arrayOfArrays.size(); i++) {
            List<Class<?>> array = arrayOfArrays.get(i);
            if (array == null) continue;
            if (array.size() > maxLength) {
                maxLength = array.size();
                index = i;
            }
        }
        return index;
    }

    /*
     * Keeping this in case it might be needed at some point, even though it's unlikely

    public static <E extends Entity, C extends Controller<E>> C constructController(Class<E> clazz, Class<C> controllerClazz) {
        try {
            Service<E> service = new Injector().injectService(clazz);
            return (C) controllerClazz.getSuperclass().getDeclaredConstructor(Service.class).newInstance(service);
        } catch (SQLException | IOException | ClassNotFoundException | InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
     */
}
