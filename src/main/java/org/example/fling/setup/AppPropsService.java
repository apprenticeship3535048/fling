package org.example.fling.setup;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AppPropsService {

    public ApplicationProps getMySqlData() throws IOException {
        Properties props = getProps();
        return new ApplicationProps(
                props.getProperty("connection"),
                props.getProperty("username"),
                props.getProperty("password")
        );
    }
    public static Properties getProps() throws IOException {
        String propsPath = "src/main/resources/application.properties";
        FileInputStream fileInputStream = new FileInputStream(propsPath);
        Properties properties = new Properties();
        properties.load(fileInputStream);

        return properties;
    }
}
