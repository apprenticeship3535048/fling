package org.example.fling.magic;

import org.example.fling.Entity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import static org.example.fling.magic.MagicUtils.getGetter;

public class JsonConverter {
    public static String convertObjectToJson(Object entity) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<Field> fields = MagicUtils.getAllFields(entity.getClass());
        StringBuilder builder = new StringBuilder();
        builder.append("{");
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            Method getter = getGetter(field, entity);
            Object val = getter.invoke(entity);
            if (isFieldNumberType(field.getType())) {
                builder
                        .append("\"")
                        .append(field.getName())
                        .append("\": ")
                        .append(val.toString());
            } else {
                builder
                        .append("\"")
                        .append(field.getName())
                        .append("\": \"")
                        .append(val)
                        .append("\"");
            }
            if (i != fields.size() - 1) {
                builder.append(",");
            }
        }
        builder.append("}");
        return builder.toString();
    }

    public static <E extends Entity> String convertListOfObjectsToJson(List<?> entities) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < entities.size(); i++) {
            E entity = (E) entities.get(i);
            builder.append(convertObjectToJson(entity));
            if (i != entities.size() - 1) {
                builder.append(",");
            }
        }
        builder.append("]");
        return builder.toString();
    }
    private static boolean isFieldNumberType(Class<?> fieldType) {
        return fieldType == byte.class ||
                fieldType == short.class ||
                fieldType == int.class ||
                fieldType == long.class ||
                fieldType == float.class ||
                fieldType == double.class ||
                fieldType == Byte.class ||
                fieldType == Short.class ||
                fieldType == Integer.class ||
                fieldType == Long.class ||
                fieldType == Float.class ||
                fieldType == Double.class;
    }
}
