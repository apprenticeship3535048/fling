package org.example.fling.setup.server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DefaultHandler implements HttpHandler {

    private final Method method;

    private final Object controller;

    public DefaultHandler(Method method, Object controller) {
        this.method = method;
        this.controller = controller;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String response;
        try {
            response = HandlerService.createResponse(controller, HandlerService.getArgs(method.getParameters(), exchange), method);
        } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
            exchange.getResponseHeaders().add("Content-Type", "application/json");
            exchange.sendResponseHeaders(200, response.getBytes().length);
            OutputStream os = exchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
    }
}
