package org.example.testProject.entities;

import org.example.fling.Entity;
import org.example.fling.annotations.entity.Column;

public class Question extends Entity {

    @Column
    private String question;

    public Question() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
