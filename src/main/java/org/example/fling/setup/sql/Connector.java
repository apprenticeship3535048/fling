package org.example.fling.setup.sql;

import org.example.fling.Entity;
import org.example.fling.annotations.entity.Column;
import org.example.fling.magic.MagicUtils;
import org.example.fling.setup.AppPropsService;
import org.example.fling.setup.ApplicationProps;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Connector {

    private final AppPropsService service;
    private final Connection connection;

    public Connector(AppPropsService service) throws SQLException, IOException, ClassNotFoundException {
        this.service = service;
        this.connection = establishConnection();
    }

    public Connection establishConnection() throws IOException, SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        ApplicationProps props = service.getMySqlData();
        return DriverManager.getConnection(props.getConnection(), props.getUsername(), props.getPassword());
    }

    public ResultSet executeQuery(String query) throws SQLException {
        Statement statement = connection.createStatement();
        return statement.executeQuery(query);
    }

    //TODO: REMOVE
    public <E extends Entity> ResultSet executeQuery(Class<E> clazz) throws SQLException {
        Statement statement = connection.createStatement();

        List<Field> fields = MagicUtils.getAllFields(clazz);
        List<String> fieldNames = new ArrayList<>();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Column.class)) {
                Column annotation = field.getAnnotation(Column.class);
                String columnName = annotation.name().isEmpty() ? field.getName() : annotation.name();
                fieldNames.add(columnName);
            }
        }
        String query = "SELECT " + String.join(", ", fieldNames) + " FROM " + clazz.getSimpleName().toLowerCase();

        return statement.executeQuery(query);
    }
}
