package org.example.fling.magic.repo;

import org.example.fling.magic.Magician;
import org.example.fling.magic.sql.RepositorySqlResolver;
import org.example.fling.setup.AppPropsService;
import org.example.fling.setup.sql.Connector;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class DynamicInvocationHandler implements InvocationHandler {

    public DynamicInvocationHandler() {

    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Connector c = new Connector(new AppPropsService());
        Class<?> entityClazz = RepositorySqlResolver.getEntity(proxy.getClass().getInterfaces()[0]);
        String query = "";
        if (args != null) {
            for (Object arg : args) {
                if (arg instanceof UUID) {
                    ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
                    byteBuffer.putLong(((UUID)args[0]).getMostSignificantBits());
                    byteBuffer.putLong(((UUID)args[0]).getLeastSignificantBits());
                    byte[] binaryUUID = byteBuffer.array();
                    StringBuilder builder = new StringBuilder();
                    builder.append("0x");
                    for (byte b : binaryUUID) {
                        builder.append(String.format("%02X", b));
                    }
                    query = RepositorySqlResolver.createQueryFromMethodAndEntityString(method, entityClazz, builder.toString());
                }
                else {
                    throw new RuntimeException("Parameter not resolvable");
                }
            }

        } else {
            query = RepositorySqlResolver.createQueryFromMethodAndEntityString(method, entityClazz);
        }
        ResultSet resultSet = c.executeQuery(query);
        Object entity = entityClazz.getDeclaredConstructor().newInstance();
        if (method.getReturnType().equals(List.class)) {
            return createForListResponseType(resultSet, entity);
        } else {
            return createForSingleResponseType(resultSet, entity);
        }
    }

    private static void addColumn(ResultSet set, Map<String, Object> map, int index) throws SQLException {
        map.put(set.getMetaData().getColumnName(index), set.getObject(index));
    }

    private static List<?> createForListResponseType(ResultSet set, Object entity) {
        try {
            List<Object> resultList = new ArrayList<>();
            while (set.next()) {
                Map<String, Object> result = new HashMap<>();
                for (int i = 1; i < set.getMetaData().getColumnCount() + 1; i++) {
                    addColumn(set, result, i);
                }
                resultList.add(Magician.buildEntityFromQueryResult(result, entity));
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }

    }

    private static Object createForSingleResponseType(ResultSet set, Object entity) {
        try {
            set.next();
            Map<String, Object> result = new HashMap<>();
            for (int i = 1; i < set.getMetaData().getColumnCount() + 1; i++) {
                addColumn(set, result, i);
            }
            return Magician.buildEntityFromQueryResult(result, entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}
