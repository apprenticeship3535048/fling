package org.example.fling.magic;

import org.example.fling.Entity;
import org.example.fling.annotations.controller.mapping.GetMapping;

import java.util.List;

public class Controller<T extends Entity> {

    private final Service<T> service;

    public Controller(Service<T> service) {
        this.service = service;
    }

    @GetMapping(mapping = "/")
    public List<T> findAll() {
        return service.findAll();
    }

    public Service<T> getService() {
        return service;
    }
}
