package org.example.testProject.service;

import org.example.testProject.entities.Answer;
import org.example.fling.annotations.FlingService;
import org.example.testProject.repo.AnswerRepo;

import java.util.List;
import java.util.UUID;

@FlingService
public class AnswerService {

    private final AnswerRepo repo;


    public AnswerService(AnswerRepo repo) {
        this.repo = repo;
    }

    public List<Answer> findAll() {
        return repo.findAll();
    }

    public Answer findById(UUID id) {
        return repo.findById(id);
    }
}
