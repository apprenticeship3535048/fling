package org.example.testProject.entities;

import org.example.fling.Entity;
import org.example.fling.annotations.entity.Column;

import java.util.UUID;

public class Answer extends Entity {
    @Column(name = "answer")
    private String answer;
    @Column
    private boolean correct;
    @Column
    private int score;
    @Column
    private UUID question_id;
    @Column
    private UUID user_id;

    public Answer() {

    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public UUID getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(UUID question_id) {
        this.question_id = question_id;
    }

    public UUID getUser_id() {
        return user_id;
    }

    public void setUser_id(UUID user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id=" + getId() +
                "answer='" + answer + '\'' +
                ", correct=" + correct +
                ", score=" + score +
                ", question_id=" + question_id +
                ", user_id=" + user_id +
                '}';
    }
}