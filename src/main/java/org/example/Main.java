package org.example;

import org.example.fling.setup.server.HttpServerConfig;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        HttpServerConfig.createServer();
    }
}