package org.example.fling.magic;

import org.example.fling.annotations.entity.Column;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static org.example.fling.magic.MagicUtils.*;

public class Magician {

    /**
     * Takes the result of a query and maps it to the according entity specified
     */
    public static Object buildEntityFromQueryResult(Map<String, Object> result, Object entity) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<Field> fields = getAllFields(entity.getClass());
        for (Field field : fields) {
            if (field.isAnnotationPresent(Column.class)) {
                Method setter = getSetter(field, entity);
                setter.setAccessible(true);
                Object obj = result.get(field.getName());
                setField(setter, entity, obj, field);
            }
        }
        return entity;
    }

    /**
     * sets the field of an object to a specific value
     */
    private static void setField(Method setter, Object entity, Object obj, Field field) throws InvocationTargetException, IllegalAccessException {

        if (field.getType() == UUID.class) {
            UUID uuid = getUUIDFromBytes((byte[]) obj);
            setter.invoke(entity, uuid);
        } else setter.invoke(entity, obj);
    }

}
