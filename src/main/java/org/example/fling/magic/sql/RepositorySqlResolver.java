package org.example.fling.magic.sql;

import org.example.fling.magic.MagicUtils;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class RepositorySqlResolver {

    public static String createQueryFromMethodAndEntityString(Method method, Class<?> entity) {
        String[] methodComponents = method.getName().split("(?=[A-Z])");
        StringBuilder queryBuilder = new StringBuilder();
        for (String component : methodComponents) {
            switch (component) {
                case "find":
                    queryBuilder.append("SELECT ");
                    break;
                case "All":
                    queryBuilder
                            .append(resolveDesiredFields(entity))
                            .append(" FROM ")
                            .append(entity.getSimpleName().toLowerCase());
                    break;
            }
        }
        System.out.println(queryBuilder);
        return queryBuilder.toString();
    }
    public static String createQueryFromMethodAndEntityString(Method method, Class<?> entity, Object arg) {
        String[] methodComponents = method.getName().split("(?=[A-Z])");
        StringBuilder queryBuilder = new StringBuilder();
        for (String component : methodComponents) {
            switch (component) {
                case "find":
                    queryBuilder.append("SELECT ");
                    break;
                case "All":
                    queryBuilder
                            .append(resolveDesiredFields(entity))
                            .append(" FROM ")
                            .append(entity.getSimpleName().toLowerCase());
                    break;
                case "By":
                    queryBuilder
                            .append(resolveDesiredFields(entity))
                            .append(" FROM ")
                            .append(entity.getSimpleName().toLowerCase())
                            .append(" WHERE ");
                    break;
                default:
                    queryBuilder
                            .append(component.toLowerCase())
                            .append(" = ")
                            .append(arg);

            }
        }
        System.out.println(queryBuilder);
        return queryBuilder.toString();
    }

    private static String resolveDesiredFields(Class<?> clazz) {
        return String.join(", ", MagicUtils.extractColumnNames(clazz));
    }

    public static Class<?> getEntity(Class<?> repository) {
        ParameterizedType parameterizedType = (ParameterizedType) repository.getGenericInterfaces()[0];
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        return ((Class<?>) actualTypeArguments[0]);
    }
}
